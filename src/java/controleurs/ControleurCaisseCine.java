package controleurs;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ControleurCaisseCine  {
    
    int     nbAdultes;
    int     nbEtudiants;     
    int     nbEnfants;
    boolean jourPromo;
    
    float   montantFacture;
    float   prixMoyenPlace;  
    
   public void faireLesCalculs() {
       
       float total    = 7f*nbAdultes + 5.5f*nbEtudiants + 4f*nbEnfants;
       if( jourPromo ) { total=total*0.8f;}
       
       int   nbPlaces = nbEtudiants+nbEnfants+nbAdultes;
 
       float prixMoyenP;
       if(nbPlaces>0){prixMoyenP=total/nbPlaces;}else{prixMoyenP=0f;}
       
       setMontantFacture(total);
       setPrixMoyenPlace(prixMoyenP);   
   }

    public void raz() {
        
        setJourPromo(false);
        setNbAdultes(0);
        setNbEnfants(0);
        setNbEtudiants(0);
        
        faireLesCalculs();
    }
   
   //<editor-fold defaultstate="collapsed" desc="Code généré">
   public int getNbAdultes() {
       return nbAdultes;
   }
   
   public void setNbAdultes(int nbAdultes) {
       this.nbAdultes = nbAdultes;
   }
   
   public int getNbEtudiants() {
       return nbEtudiants;
   }
   
   public void setNbEtudiants(int nbEtudiants) {
       this.nbEtudiants = nbEtudiants;
   }
   
   public int getNbEnfants() {
       return nbEnfants;
   }
   
   public void setNbEnfants(int nbEnfants) {
       this.nbEnfants = nbEnfants;
   }
   
   public boolean isJourPromo() {
       return jourPromo;
   }
   
   public void setJourPromo(boolean jourPromo) {
       this.jourPromo = jourPromo;
   }
   
   public float getMontantFacture() {
       return montantFacture;
   }
   
   public void setMontantFacture(float montantFacture) {
       this.montantFacture = montantFacture;
   }
   
   public float getPrixMoyenPlace() {
       return prixMoyenPlace;
   }
   
   public void setPrixMoyenPlace(float prixMoyenPlace) {
       this.prixMoyenPlace = prixMoyenPlace;
   }
   
   
   //</editor-fold>
}



